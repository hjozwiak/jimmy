# Jimmy, a Command Line Interface to the Digital Ocean API
## Introduction
This program is designed to be a user-friendly command line interface to 
the Digital Ocean API, consisting of an easy to use text interface. This 
application will be written in the Clojure programming language, and 
will be taking advantage of some of the third party Clojars, namely 
digitalocean version 1.2.
## Contributions
If you would like to contribute to the project, please feel free to do 
so by sending a pull request or by filing an issue.
## Donations
While not required, donations are greatly appreciated. You may pay in  
with Monero, by sending your donations to the address 
49ySqd3Rsb7ZRPcPmBFrvmYQ6tk6U8z63Uok8NSUwtwR3WBLaAew9UtbKqB2R8fg7h31WR7RpcvnMQAoTQSE2W1818kB4Ki 

